package com.gustavodev.booksearcher.domain.model

data class Book(
  val id: String,
  val name: String,
  val author: String,
  val imageSource: String,
  val rate: Float)