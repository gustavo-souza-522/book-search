package com.gustavodev.booksearcher.datasource.api.bookapi.mapper

import com.gustavodev.booksearcher.datasource.api.bookapi.model.BookEntity
import com.gustavodev.booksearcher.datasource.api.builder.interfaces.EntityMapper
import com.gustavodev.booksearcher.domain.model.Book

class BookApiMapper : EntityMapper<BookEntity, Book> {

  override fun entityToModel(entity: BookEntity): Book {
    return Book(entity.id, entity.name, entity.author, entity.imageSource, entity.rate ?: 0f)
  }

  override fun entityListToModelList(entities: List<BookEntity>): List<Book> {
    val models: ArrayList<Book> = ArrayList()

    for (entity in entities) models.add(entityToModel(entity = entity))

    return models
  }

  override fun modelToEntity(model: Book): BookEntity {
    return BookEntity(model.id, model.name, model.author, model.imageSource, model.rate ?: 0f)
  }

  override fun modelListToEntityList(models: List<Book>): List<BookEntity> {
    val entities: ArrayList<BookEntity> = ArrayList()

    for (model in models) entities.add(modelToEntity(model = model))

    return entities
  }
}