package com.gustavodev.booksearcher.datasource.api.bookapi.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class BookEntity(
  @Expose
  @SerializedName("id")
  val id: String,

  @Expose
  @SerializedName("name")
  val name: String,

  @Expose
  @SerializedName("author")
  val author: String,

  @Expose
  @SerializedName("image_src")
  val imageSource: String,

  @Expose
  @SerializedName("aval")
  val rate: Float? = null
)