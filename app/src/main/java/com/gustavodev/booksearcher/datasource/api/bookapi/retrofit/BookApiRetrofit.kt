package com.gustavodev.booksearcher.datasource.api.bookapi.retrofit

import com.gustavodev.booksearcher.datasource.api.bookapi.model.BookEntity
import com.gustavodev.booksearcher.domain.model.Book
import kotlinx.coroutines.flow.Flow
import retrofit2.http.GET
import retrofit2.http.Query

/**
 * Book api retrofit. It makes HTTP requests.
 *
 * @author Gustavo Souza
 */
interface BookApiRetrofit {

  /**
   * Makes a GET HTTP request to search a book.
   *
   * @return The search result
   */
  @GET
  fun search(@Query(value = "bookName") bookName: String): Flow<List<BookEntity>>

}