package com.gustavodev.booksearcher.datasource.api.builder

import com.gustavodev.booksearcher.datasource.api.bookapi.retrofit.BookApiRetrofit
import com.gustavodev.booksearcher.datasource.api.builder.adapter.FlowCallAdapterFactory
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object ApiBuilder {

  private const val BASE_URL = "http:192.168.25.8:8080/"

  private val retrofitBuilder: Retrofit.Builder by lazy {
    Retrofit.Builder()
      .baseUrl(BASE_URL)
      .addConverterFactory(GsonConverterFactory.create())
      .addCallAdapterFactory(FlowCallAdapterFactory.create())
  }

  val bookApiRetrofit: BookApiRetrofit by lazy {
    retrofitBuilder
      .build()
      .create(BookApiRetrofit::class.java)
  }

}