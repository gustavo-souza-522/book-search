package com.gustavodev.booksearcher.datasource.api.bookapi

import com.gustavodev.booksearcher.data.api.BookSearchApi
import com.gustavodev.booksearcher.datasource.api.bookapi.mapper.BookApiMapper
import com.gustavodev.booksearcher.datasource.api.bookapi.retrofit.BookApiRetrofit
import com.gustavodev.booksearcher.domain.model.Book
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.flow

class BookApiServiceImpl (
  private val bookApiRetrofit: BookApiRetrofit,
  private val bookApiMapper: BookApiMapper
): BookSearchApi {

  /**
   * Makes a search from the API
   */
  override suspend fun search(query: String): Flow<List<Book>> = flow {
    bookApiRetrofit.search(query).collect {  bookEntities ->
      val bookModels: List<Book> = bookApiMapper.entityListToModelList(bookEntities)
      emit(bookModels)
    }
  }
}