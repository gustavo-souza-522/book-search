package com.gustavodev.booksearcher.datasource.api.builder.interfaces

interface EntityMapper<Entity, Model> {

  fun entityToModel(entity: Entity): Model
  fun entityListToModelList(entities: List<Entity>): List<Model>

  fun modelToEntity(model: Model): Entity
  fun modelListToEntityList(models: List<Model>): List<Entity>

}