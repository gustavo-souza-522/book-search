package com.gustavodev.booksearcher.datasource.api.bookapi

import com.gustavodev.booksearcher.domain.model.Book

interface BookApiService {

  fun search(bookName: String): List<Book>

}