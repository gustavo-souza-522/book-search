package com.gustavodev.booksearcher.core

/**
 * A wrapper data class to get data for only one time.
 */
class SingleData<T>(private val data: T) {

  var consumed: Boolean = false
    private set

  /**
   * Get the data if not consumed. Else null.
   *
   * @return Data if not consumed, else null.
   *
   * @see getDataEvenIfWasConsumed
   */
  fun getData(): T? {
    if (consumed) return null

    consumed = true
    return data
  }

  /**
   * Get the data even if it was consumed.
   *
   * @return Data
   *
   * @see getData
   */
  fun getDataEvenIfWasConsumed(): T = data

  companion object {

    // We don't want an event if there is no data.
    fun <T> dataEvent(data: T?): SingleData<T>? {
      data?.let { return SingleData(it) }
      return null
    }

    // We don't want an event if there is no message.
    fun messageEvent(message: String?): SingleData<String>? {
      message?.let { return SingleData(message) }
      return null
    }

  }

}