package com.gustavodev.booksearcher.core

/**
 * The data state of a presentation data.
 */
data class DataStateOf<PresentationData>(
  var message : SingleData<String>? = null,
  var loading : Boolean = false,
  var data    : SingleData<PresentationData>? = null
) {

  companion object {

    /**
     * Get an state of type Data, with message and data.
     */
    fun <PresentationData> success(message: String? = null, data: PresentationData? = null)
      : DataStateOf<PresentationData> {
      return DataStateOf(
        message = SingleData.messageEvent(message),
        loading = false,
        data    = SingleData.dataEvent(data)
      )
    }

    /**
     * Get an state of type Loading, with message and data.
     */
    fun <PresentationData> loading(isLoading: Boolean)
      : DataStateOf<PresentationData> {
      return DataStateOf(loading = isLoading)
    }

    /**
     * Get an state of type Error, with message and data.
     */
    fun <PresentationData> error(message: String)
      : DataStateOf<PresentationData> {
      return DataStateOf(message = SingleData(message))
    }

  }

  override fun toString(): String {
    return "DataState(message=$message, loading=$loading, data=$data)"
  }

}