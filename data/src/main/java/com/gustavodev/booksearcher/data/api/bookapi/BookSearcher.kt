package com.gustavodev.booksearcher.data.api.bookapi

import com.gustavodev.booksearcher.domain.model.Book
import kotlinx.coroutines.flow.Flow

interface BookSearcher {

  suspend fun search(query: String): Flow<List<Book>>

}