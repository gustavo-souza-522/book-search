package com.gustavodev.booksearcher.data.api

import com.gustavodev.booksearcher.domain.model.Book
import kotlinx.coroutines.flow.Flow

/**
 * Book API. It contains methods to get from the API.
 *
 * @author Gustavo Souza
 */
interface BookSearchApi {

  /**
   * Makes a search from the API
   */
  suspend fun search(query: String): Flow<List<Book>>

}