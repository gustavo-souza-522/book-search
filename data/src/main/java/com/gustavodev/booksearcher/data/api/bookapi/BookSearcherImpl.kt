package com.gustavodev.booksearcher.data.api.bookapi

import com.gustavodev.booksearcher.data.api.BookSearchApi
import com.gustavodev.booksearcher.domain.model.Book
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.launchIn

class BookSearcherImpl(
  private val bookSearchApi: BookSearchApi,
  private val scope: CoroutineScope
) : BookSearcher {

  override suspend fun search(query: String): Flow<List<Book>>
    = bookSearchApi.search(query).launchIn(scope)


}